ARG PARENT_IMAGE
FROM $PARENT_IMAGE

ARG DOCKER_VERSION
ENV DOCKER_VERSION=$DOCKER_VERSION

ARG MAVEN_VERSION
ENV MAVEN_VERSION=$MAVEN_VERSION

ARG GRADLE_VERSION
ENV GRADLE_VERSION=$GRADLE_VERSION

ARG SBT_VERSION
ENV SBT_VERSION=$SBT_VERSION

ARG ANT_VERSION
ENV ANT_VERSION=$ANT_VERSION
ENV ANT_HOME="/ant-$ANT_VERSION-bin"

LABEL maintainer="gitlab@therack.io"
LABEL description="Java build tools container"
LABEL docker_version="${DOCKER_VERSION}"
LABEL maven_version="${MAVEN_VERSION}"
LABEL gradle_version="${GRADLE_VERSION}"
LABEL sbt_version="${SBT_VERSION}"
LABEL ant_version="${ANT_VERSION}"

WORKDIR /

# Install Docker Engine
ADD https://download.docker.com/linux/static/stable/aarch64/docker-$DOCKER_VERSION.tgz /

RUN \
    set -eux && \
    apt update && \
    apt install --no-install-recommends -y git wget curl unzip && \
    apt -qq -y -o Dpkg::Options::="--force-confold" full-upgrade && \
    apt-get clean && \
    rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/* && \
    tar -xzf docker-$DOCKER_VERSION.tgz && \
    rm -rf docker-$DOCKER_VERSION.tgz && \
    cp docker/* /usr/bin/ && \
    chmod +x /usr/bin/docker* && \
    rm -rf ./docker && \
    docker --version 


# Install Maven
ADD https://s3.wasabisys.com/therackio-releases/therackio/build-tools/binaries/apache-maven-bin-arm64/apache-maven-$MAVEN_VERSION-bin.tar.gz /

RUN \
    set -eux && \
    cd /  && \
    mkdir -p /opt/maven && \
    tar -xzf ./apache-maven-$MAVEN_VERSION-bin.tar.gz -C /opt/maven && \
    rm -rf ./apache-maven-$MAVEN_VERSION-bin.tar.gz 

ENV PATH="/opt/maven/apache-maven-$MAVEN_VERSION/bin:${PATH}"

RUN mvn -version


# Install Gradle
ADD https://s3.wasabisys.com/therackio-releases/therackio/build-tools/binaries/gradle-bin-arm64/gradle-$GRADLE_VERSION-bin.tar.gz /

RUN \
    set -eux && \
    cd /  && \
    tar -xzf ./gradle-$GRADLE_VERSION-bin.tar.gz -C /opt && \
    rm -rf ./gradle-$GRADLE_VERSION-bin.tar.gz && \
    ls -al /opt/gradle* && \
    cd /opt/gradle-$GRADLE_VERSION* && \
    ls -al && \
    pwd && \
    export GRADLE_PATH=$(pwd) && \
    echo $GRADLE_PATH && \
    mkdir -p /opt/gradle/ && \
    ln -s $GRADLE_PATH /opt/gradle/gradle-$GRADLE_VERSION && \
    ls -al /opt && \
    ls -al /opt/gradle && \
    ls -al /opt/gradle/gradle-$GRADLE_VERSION

ENV PATH="/opt/gradle/gradle-$GRADLE_VERSION/bin:${PATH}"

RUN gradle --version


# Install SBT
ADD https://github.com/sbt/sbt/releases/download/v$SBT_VERSION/sbt-$SBT_VERSION.tgz /

RUN \
    set -eux && \
    cd / && \
    tar -xzf sbt-$SBT_VERSION.tgz && \
    rm -rf sbt-$SBT_VERSION.tgz && \
    ls -al /sbt

ENV PATH="/sbt/bin:${PATH}"

RUN \
    sbt -Dsbt.rootdir=true sbtVersion


# Install Ant
RUN \
    set -eux && \
    git clone https://github.com/apache/ant.git /ant && \
    cd /ant && \
    git fetch && \
    git checkout tags/rel/$ANT_VERSION && \
    chmod +x ./build.sh && \
    sh build.sh install-lite && \
    ls -al $ANT_HOME 

ENV PATH="${ANT_HOME}/bin:${PATH}"

RUN ant -version

# Install latest AWS CLI v2
RUN \
    curl "https://awscli.amazonaws.com/awscli-exe-linux-aarch64.zip" -o "awscliv2.zip" && \
    unzip awscliv2.zip && \
    rm -rf ./awscliv2.zip && \
    ./aws/install && \
    aws --version


